from django import forms
from receipts.models import Receipt, ExpenseCategory, Account

##################################################################################################

class ReceiptForm(forms.ModelForm):    #we define a django model form class by writing our own class that inherits from the django.forms.ModelForm class
    class Meta:   #need to create an inner class named Meta
        model = Receipt  #We specify which Django model it should work/pull from.
        fields = [     #specify which fields we want to show.
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

##################################################################################################

class ExpenseForm(forms.ModelForm):    #we define a django model form class by writing our own class that inherits from the django.forms.ModelForm class
    class Meta:   #need to create an inner class named Meta
        model = ExpenseCategory  #We specify which Django model it should work/pull from.
        fields = [     #specify which fields we want to show.
            "name"
        ]

##################################################################################################

class AccountForm(forms.ModelForm):    #we define a django model form class by writing our own class that inherits from the django.forms.ModelForm class
    class Meta:   #need to create an inner class named Meta
        model = Account  #We specify which Django model it should work/pull from.
        fields = [     #specify which fields we want to show.
            "name",
            "number"
        ]
