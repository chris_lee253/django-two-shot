from django.db import models
from django.conf import settings

# Create your models here.

class ExpenseCategory(models.Model): #is a value that we can apply to receipts like "gas" or "entertainment"
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories", #The related_name attribute specifies the name of the reverse relation from the User model back to your model
        on_delete=models.CASCADE,
    )

#############################################################################################################################################################################

class Account(models.Model): #reflects the method in which we paid for something - such as specific credit card or bank account
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

#############################################################################################################################################################################


class Receipt(models.Model): #primary thing that this application keeps track of for accounting purposes
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(
        max_digits=10,
        decimal_places=3,
    )
    tax = models.DecimalField(
        max_digits=10,
        decimal_places=3,
    )
    date = models.DateTimeField(auto_now_add=False)

    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )

    category = models.ForeignKey(
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE
    )

    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
